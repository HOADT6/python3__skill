Vietnamese living in the Canadian capital Ottawa are fearful of the anti-vaccine truck protests and say the so-called Freedom Convoy has disrupted daily life and impacted their mental health.
Nguyen Van Tho, 43, who has been living in Canada for five years, has been unable to sleep for the past few days due to the constant honking at night outside his apartment.

The IT worker says the trucks-led demonstration has caused traffic, noise and safety issues, affecting many of his daily routines.